# Frontend Angular



# Pour get:

curl http://localhost:3000/evaluations

# Add boisson
curl --location --request POST http://localhost:3000/boissons --header 'Content-Type:application/x-www-form-urlencoded' --data-urlencode text=victor

# Add evaluation
curl --location --request POST http://localhost:3000/evaluations --header 'Content-Type:application/x-www-form-urlencoded' --data-urlencode note=3 --data-urlencode lieu=cyclo --data-urlencode commentaire=nice --data-urlencode details=guiness --data-urlencode boissonId=1


# Delete evaluation
curl -X DELETE http://localhost:3000/evaluation/1
