import { Component, OnInit } from '@angular/core';
import { Drink } from '../drink';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { NgForm }   from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { faCocktail } from '@fortawesome/free-solid-svg-icons';



@Component({
  selector: 'app-drinks',
  templateUrl: './drinks.component.html',
  styleUrls: ['./drinks.component.css']
})
export class DrinksComponent implements OnInit {

   faCocktail = faCocktail;

   boissons = [];

  //TODO request GET /boissons
  drinks: Drink[] = [];

  index = 4;

  selectedDrink: Drink;

  //Validator
  angForm: FormGroup;
   constructor(private fb: FormBuilder, private http: HttpClient ) {
    this.createForm();
  }
  createForm() {
    this.angForm = this.fb.group({
       name: ['', Validators.required ]
    });
  }

  updateDrinks(){
    this.http.get("http://localhost:3000/boissons")
                .subscribe((data : any[]) => {
                  this.drinks = data;
                });
  }

  removeDrink(data){
    this.drinks = Object.values(data);
    this.selectedDrink = this.drinks[0];
  }

  ngOnInit() {
    this.updateDrinks();
  }

  onSelect(drink: Drink): void{
    this.selectedDrink = drink;
  }

  addDrink(f: NgForm) {
    //console.log(f.value);
    let body = new URLSearchParams();
    body.set('text', f.value['name']);

    let options = {
        headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };

    this.http
        .post('http://localhost:3000/boissons', body.toString(), options)
        .subscribe();

    this.updateDrinks();

    //this.drinks.push({id: this.index++, name: f.value['name']});
    f.form.reset();
  }

}
