import {Component, OnInit, ViewChild, Input,  Output, EventEmitter} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { Evaluation } from '../evaluation';
import {NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';
import { Drink } from '../drink';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-evaluations',
  templateUrl: './evaluations.component.html',
  styleUrls: ['./evaluations.component.css']
})
export class EvaluationsComponent implements OnInit {

  @Input() drink: Drink;
  @Output() updateDrink = new EventEmitter();

  myurl: string = "";

  //TODO get evaluation by drink

  evaluations: Evaluation[] = [
  {boisson_id: 1, titre: 'Excellent thé rafraîchissant', date : '04.01.2020', note: 4, lieu: 'Belvédère Fribourg', commentaire: 'Très bon thé avec des Glaçons'},
  {boisson_id: 1, titre: 'Excellent thé rafraîchissant', date : '04.01.2020', note: 2, lieu: 'Belvédère Fribourg', commentaire: 'Très bon thé avec des Glaçons'},
  {boisson_id: 1, titre: 'Excellent thé rafraîchissant', date : '04.01.2020', note: 1, lieu: 'Belvédère Fribourg', commentaire: 'Très bon thé avec des Glaçons'},
];

  updateEvaluations(){
    //console.log(this.drink.id);
    //this.myurl = "http://localhost:3000/evaluations/" + this.drink.id;

    this.myurl = "http://localhost:3000/evaluations";
    console.log(this.myurl);
    this.http.get(this.myurl)
              .subscribe((data : any[]) => {
                console.log(data);
                //Parcourir les data pour add uniquement les drink.id = boisson_id
                this.evaluations = data;
              })

    this.updateDrink.emit();
    }

  ngOnInit() {
    this.updateEvaluations();
  }

  constructor(config: NgbRatingConfig, private http: HttpClient) {
    // customize default values of ratings used by this component tree
    config.max = 5;
    config.readonly = true;
  }

}
