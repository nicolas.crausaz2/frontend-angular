import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { NgForm }   from '@angular/forms';
import { Drink } from '../drink';
import { Evaluation } from '../evaluation';
import {formatDate} from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-evaluation-add',
  templateUrl: './evaluation-add.component.html',
  styleUrls: ['./evaluation-add.component.css']
})
export class EvaluationAddComponent implements OnInit {

    @Input() drink: Drink;
    @Output() updateEvent = new EventEmitter();
    new_evaluation: Evaluation;

    selected = 0;   //Rating

    ngOnInit(){

    }

    //Validator
    angForm: FormGroup;
     constructor(private fb: FormBuilder, private http: HttpClient) {
      this.createForm();
    }
    createForm() {
      this.angForm = this.fb.group({
         titre: ['', Validators.required ],
         lieu: ['', Validators.required ],
         comment: ['', Validators.required ]
      });
    }


    addEvent(f: NgForm) {

      let body = new URLSearchParams();

      this.new_evaluation  = {boisson_id: this.drink.id, titre: f.value['titre'], date : formatDate(new Date(), 'dd.MM.yyyy', 'en'), note: this.selected, lieu:  f.value['lieu'], commentaire: f.value['comment']};

      body.set('note', String(this.selected));
      body.set('lieu', f.value['lieu']);
      body.set('commentaire', f.value['comment']);
      body.set('titre', f.value['titre']);
      body.set('date',formatDate(new Date(), 'dd.MM.yyyy', 'en'));
      body.set('boissonId', String(this.drink.id));

      let options = {
          headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
      };

      this.http
          .post('http://localhost:3000/evaluations', body.toString(), options)
          .subscribe(response => {
              console.log(response);
          });

      //Add countEval
      let body2 = new URLSearchParams();
      this.drink.countEval = +this.drink.countEval + 1;

      //console.log("Before " + String(this.drink.countEval));
      //console.log(typeof(this.drink.countEval));

      body2.set('id', String(this.drink.id));
      body2.set('text', this.drink.name);
      body2.set('countEval', String(this.drink.countEval));


      let options2 = {
          headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
      };

      this.http
          .post('http://localhost:3000/updateBoisson', body2.toString(), options2)
          .subscribe(response => {
              console.log(response);
              //CALL updateDrinks
          });


      f.form.reset();

      this.updateEvent.emit();
    }
  }
