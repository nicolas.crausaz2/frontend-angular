import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Drink } from '../drink';

import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-drink-detail',
  templateUrl: './drink-detail.component.html',
  styleUrls: ['./drink-detail.component.css']
})

export class DrinkDetailComponent implements OnInit {

  @Input() drink: Drink;
  @Output() updateDrink = new EventEmitter<any>();

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }


  saveDrink(){
    let body = new URLSearchParams();

    body.set('id', String(this.drink.id));
    body.set('text', this.drink.name);
    body.set('countEval', String(this.drink.countEval));


    let options = {
        headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };

    this.http
        .post('http://localhost:3000/updateBoisson', body.toString(), options)
        .subscribe(response => {
            console.log(response);
        });
  }


  deleteDrink(){
    this.http.delete("http://localhost:3000/boissons/" + this.drink.id).subscribe(
      data =>{
        this.updateDrink.emit(data);
    });
  }


}
