export class Evaluation{
  boisson_id: number;
  titre: string;
  date: string;
  note: number;
  lieu: string;
  commentaire: string;
}
