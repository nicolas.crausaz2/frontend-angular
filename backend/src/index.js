import express from 'express';
import uuidv4 from 'uuid/v4';




const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
var cors = require('cors');
app.use(cors());

app.use((req, res, next) => {
  req.me = boissons[1];
  next();
});


let boissons = {
  1: {
    id: '1',
    name: 'Bière rousse',
    countEval: 2,
  },
  2: {
    id: '2',
    name: 'Sirop',
    countEval: 1,
  },
};

let evaluations = {
  1: {
    id: '1',
    note: '4',
    lieu: 'AG',
    commentaire: 'Pleine de caractère',
    titre: 'Très bonne',
    date: '08.01.2020',
    boissonId: '1',
  },
  2: {
    id: '2',
    note: '3',
    lieu: 'Commerce',
    commentaire: 'Pas assez de gaz',
    titre: 'Moyennne',
    date: '08.01.2020',
    boissonId: '1',
  },
  3: {
    id: '3',
    note: '5',
    lieu: 'Commerce',
    commentaire: 'Sirop framboise fait maison ! Nickel',
    titre: 'Parfait',
    date: '08.01.2020',
    boissonId: '2',
  },
};


app.get('/boissons', (req, res) => {
  return res.send(Object.values(boissons));
});
app.get('/boissons/:boissonId', (req, res) => {
  return res.send(boissons[req.params.boissonId]);
});

app.post('/boissons', (req, res) => {
  const id = uuidv4();
  const boisson = {
    id,
    name: req.body.text,
    countEval: 0,
  };
  boissons[id] = boisson;
  return res.send(boisson);
});


app.post('/updateBoisson', (req, res) => {
  const id = req.body.id;
  const boisson = {
    id,
    name: req.body.text,
    countEval: req.body.countEval,
  };
  boissons[id] = boisson;
  return res.send(boisson);
});


app.delete('/boissons/:boissonId', (req, res) => {
  const {
    [req.params.boissonId]: boisson,
    ...otherBoissons
  } = boissons;
  boissons = otherBoissons;
  return res.send(boissons);
});


app.get('/evaluations', (req, res) => {
  return res.send(Object.values(evaluations));
});
app.get('/evaluations/:evaluationId', (req, res) => {
  return res.send(evaluations[req.params.evaluationId]);
});

app.post('/evaluations', (req, res) => {
  const id = uuidv4();
  const evaluation = {
    id,
    note: req.body.note,
    lieu: req.body.lieu,
    commentaire: req.body.commentaire,
    titre: req.body.titre,
	date: req.body.date,
    boissonId: req.body.boissonId,
  };
  evaluations[id] = evaluation;
  return res.send(evaluation);
});

app.delete('/evaluations/:evaluationId', (req, res) => {
  const {
    [req.params.evaluationId]: evaluation,
    ...otherEvaluations
  } = evaluations;
  evaluations = otherEvaluations;
  return res.send(evaluation);
});


app.listen(3000, () =>
  console.log('Example app listening on port 3000!'),
);
